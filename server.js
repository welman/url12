const restify = require('restify');
var errs = require('restify-errors');
const corsMiddleware = require('restify-cors-middleware');
const config = require('./src/config/config');
const log = require('./src/services/logger');
const urlService = require('./src/services/urlService');

//
// Log application errors
//
process.on("error", () => {
    log.error(arguments);
});

const server = restify.createServer({
    name: 'Url shortener'
});


//
// Setup middlewares
//
const cors = corsMiddleware({
    preflightMaxAge: 5,
    origins: ['*']
});
server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

//
// Default response
// TODO: To move as Route
//
server.get('/', (req, res, next) => {
    log.info('Returning about info response');
    res.send({ about: 'This is url shortener api service', urlService: config.urlService || 'opps' });
    return next();
});

//Healthcheck response to make Azure App Service happy
server.get('/robots933456.txt', (req, res, next) => {
    log.info('Returning healthcheck response: robots933456.txt');
    res.send();
    return next();
});

//
// Redirecting the browser from Short-URL into target Long-URL site.
// TODO: To move as Route
//
server.get('/:hash(^[a-zA-Z0-9]+$)', async (req, res, next) => {
    try {
        const hash = req.params.hash;
        log.info('Received hash value', hash);

        const longUrl = await urlService.resolveLongUrl(hash);
        log.info('Resolved long Url', longUrl);

        if(longUrl) {
            log.info('Redirecting to long Url', longUrl);
            res.redirect(301, longUrl, next);   //Redirect Permanently
        }
        else {
            log.info('Returning Not Found status for hash', hash);
            res.status(404);    //not found
        }

        res.send();
        return next();

    } catch (e) {
        return next(e);
    }
})


//
// Creates new short url. Accepts long Url
//
server.put('/shorturl_', async (req, res, next) => {
    try {
        const data = req.body;
        log.info('Received data for shortening', data);

        if(!data || !data.longUrl) {
            log.info('Returning Bad Request due to missing long Url data.');
            return next(new errs.BadRequestError('URL value is Required.'));
        }

        const hash = await urlService.shortenUrl(data.longUrl);

        if(!hash) {
            log.info('Returning Internal Server Error due to not able to shorten the url successfully, for url', data.longUrl);
            return next(new errs.BadRequestError('URL value is Invalid'));
        }

        const responseData = {
            shortUrl: `${config.urlService}${hash}`,
            longUrl: data.longUrl
        };
        log.info('Returning successfully shortened url: ', responseData);
        res.send(responseData);
        return next();

    } catch(e) {
        return next(e);
    }
})

//
// Error Handlers
//
server.on('uncaughtException', (req, res, route, err) => {
    log.error(err.message, {
        event: 'uncaughtException'
    });

    res.send(500, {
        handler: err
    });
});

server.listen(config.port, () => {
    log.info(`${server.name} listening at ${server.url}`);
});