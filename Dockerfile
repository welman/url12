FROM node:8.11.3-jessie
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "./"]
RUN npm install --production 
RUN mv node_modules ../
COPY . .
EXPOSE 3004
CMD npm start