
let localConfig = null;
try {
    localConfig = require('./config.local'); 
} catch (ex) {
    // ignore, prod doesn't have local config
}

module.exports = Object.assign({
    urlService: process.env.URL_SERVICE,
    port: parseInt(process.env.PORT, 10) || 3000,
    connectionString: process.env.CONNECTION_STRING

}, localConfig);




