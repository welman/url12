
const chai = require('chai');
const expect = chai.expect;
const converter = require('../baseConverter');

describe('baseConverter', () => {

    describe('toBase function', () => {
        let input,
            toBaseCharList,
            result,
            expectedResult;

        beforeEach(() => {
            toBaseCharList = converter.BASE_62_CHARS;
            input = 1234;
            expectedResult = 'Ju';
        });

        it('When given base10 value, should convert to base62', () => {
            result = converter.toBase(input, toBaseCharList);
            expect(result).to.equal(expectedResult);
        })

        describe('parameter input value', () => {

            it('When input is 0, should return 0', () => {
                input = 0;
                expectedResult = '0';
                result = converter.toBase(input, toBaseCharList);
                expect(result).to.equal(expectedResult);
            })

            it('When input is null, should return 0', () => {
                input = null;
                expectedResult = '0';
                result = converter.toBase(input, toBaseCharList);
                expect(result).to.equal(expectedResult);
            })

            it('When input is undefined, should return 0', () => {
                input = undefined;
                expectedResult = '0';
                result = converter.toBase(input, toBaseCharList);
                expect(result).to.equal(expectedResult);
            })

            it('When input is negative, should return 0', () => {
                input = -1;
                expectedResult = '0';
                result = converter.toBase(input, toBaseCharList);
                expect(result).to.equal(expectedResult);
            })
        })

        describe('parameter toBaseCharList value', () => {

            it('When target base chars is null, should return null', () => {
                toBaseCharList = null;
                result = converter.toBase(input, toBaseCharList);
                expect(result).to.null;
            })

            it('When target base chars is undefined, should default to Base62', () => {
                toBaseCharList = undefined;
                result = converter.toBase(input, toBaseCharList);
                expect(result).to.equal(expectedResult);
            })

            it('When target base chars has one char, should return null', () => {
                toBaseCharList = 'r';
                result = converter.toBase(input, toBaseCharList);
                expect(result).to.null;
            })

            it('When target base chars is empty, should return null', () => {
                toBaseCharList = '';
                result = converter.toBase(input, toBaseCharList);
                expect(result).to.null;
            })

        })

    })

    describe('toNumber function', () => {
        let input,
            toBaseCharList,
            result,
            expectedResult;

        beforeEach(() => {
            toBaseCharList = converter.BASE_62_CHARS;
            input = 'Ju';
            expectedResult = 1234;
        });

        it('When given base10 value, should convert to base62', () => {
            result = converter.toNumber(input, toBaseCharList);
            expect(result).to.equal(expectedResult);
        })

        describe('parameter input value', () => {

            it('When input is 0, should return 0', () => {
                input = '0';
                expectedResult = 0;
                result = converter.toNumber(input, toBaseCharList);
                expect(result).to.equal(expectedResult);
            })

            it('When input is null, should return 0', () => {
                input = null;
                expectedResult = 0;
                result = converter.toNumber(input, toBaseCharList);
                expect(result).to.null;
            })

            it('When input is undefined, should return 0', () => {
                input = undefined;
                expectedResult = 0;
                result = converter.toNumber(input, toBaseCharList);
                expect(result).to.null;
            })

            it('When input is undefined, should return 0', () => {
                input = undefined;
                expectedResult = 0;
                result = converter.toNumber(input, toBaseCharList);
                expect(result).to.null;
            })

        })

        describe('parameter toBaseCharList value', () => {

            it('When target base chars is null, should return null', () => {
                toBaseCharList = null;
                result = converter.toNumber(input, toBaseCharList);
                expect(result).to.null;
            })
    
            it('When target base chars is undefined, should return null', () => {
                toBaseCharList = undefined;
                result = converter.toNumber(input, toBaseCharList);
                expect(result).to.equal(expectedResult);
            })
    
            it('When target base chars has one char, should return null', () => {
                toBaseCharList = 'r';
                result = converter.toNumber(input, toBaseCharList);
                expect(result).to.null;
            })
    
            it('When target base chars is empty, should return null', () => {
                toBaseCharList = '';
                result = converter.toNumber(input, toBaseCharList);
                expect(result).to.null;
            })
    
        })
 
    })

    describe('Convert number to Base62 and back', () => {
        let inputNumber,
            inputBase62,
            toBaseCharList,
            resultNumber,
            resultBase62;

        beforeEach(() => {
            toBaseCharList = converter.BASE_62_CHARS;
        });

        it('Convert', () => {
            inputNumber = 0;
            resultBase62 = converter.toBase(inputNumber, toBaseCharList);
            resultNumber = converter.toNumber(resultBase62, toBaseCharList);
            expect(resultNumber).to.equal(inputNumber);
        })

        it('Convert', () => {
            inputNumber = 12312313;
            resultBase62 = converter.toBase(inputNumber, toBaseCharList);
            resultNumber = converter.toNumber(resultBase62, toBaseCharList);
            expect(resultNumber).to.equal(inputNumber);
        })

        it('Convert', () => {
            inputNumber = 234243234234234234;
            resultBase62 = converter.toBase(inputNumber, toBaseCharList);
            resultNumber = converter.toNumber(resultBase62, toBaseCharList);
            expect(resultNumber).to.equal(inputNumber);
        })



        it('Convert', () => {
            inputBase62 = '0';
            resultNumber = converter.toNumber(inputBase62, toBaseCharList);
            resultBase62 = converter.toBase(resultNumber, toBaseCharList);
            expect(resultBase62).to.equal(inputBase62);
        })

        it('Convert', () => {
            inputBase62 = '1';
            resultNumber = converter.toNumber(inputBase62, toBaseCharList);
            resultBase62 = converter.toBase(resultNumber, toBaseCharList);
            expect(resultBase62).to.equal(inputBase62);
        })

        it('Convert', () => {
            inputBase62 = 'adA019dF';
            resultNumber = converter.toNumber(inputBase62, toBaseCharList);
            resultBase62 = converter.toBase(resultNumber, toBaseCharList);
            expect(resultBase62).to.equal(inputBase62);
        })
        
        it('Convert', () => {
            inputBase62 = 'zzzzzzzzy';
            resultNumber = converter.toNumber(inputBase62, toBaseCharList);
            resultBase62 = converter.toBase(resultNumber, toBaseCharList);
            expect(resultBase62).to.equal(inputBase62);
        })
 
    })


});