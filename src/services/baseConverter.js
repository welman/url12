//Base62 character sets
const BASE_62_CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

//Converts number (Base10) value into BaseXX equivalent (defaults to Base62).
const toBase = (input, targetBaseChars = BASE_62_CHARS) => {

    //Validates the inputs
    if(typeof targetBaseChars !== 'string' || targetBaseChars.length <= 1)
        return null;

    input = parseInt(input);
    if(isNaN(input) || input < 0)
        input = 0;

    let result = '';
    const targetBaseLength = targetBaseChars.length;

    do {
        result = `${targetBaseChars[parseInt(input % targetBaseLength)]}${result}`;
        input = parseInt(input / targetBaseLength);
    } 
    while (input > 0);

    return result;
};

//Converts BaseXX (defaults to Base62) value into number equivalent value
const toNumber = (input, baseChars = BASE_62_CHARS) => {

    //Validates inputs
    if(typeof baseChars !== 'string' || baseChars.length <= 1)
        return null;

    if(typeof input !== 'string' )
        return null;

    const baseCharsLength = baseChars.length;
    let result = 0;
    const r = input.split('').reverse().join('');

    for (let i = 0; i < r.length; i++) {
        let charIndex = baseChars.indexOf(r[i]);
        result += charIndex * Math.pow(baseCharsLength, i);
    }

    return result;
};

module.exports = {
    BASE_62_CHARS,
    toBase,
    toNumber,
}