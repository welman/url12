const sql = require('mssql');
const config = require('../config/config');
const log = require('./logger');

//Gets URL data from DB given the ID
const getById = async (id) => {

    log.info('Getting data from DB for Url with ID ', id);

    let result = null;
    try {
        const pool = await sql.connect(config.connectionString);
        const data = await pool.request()
            .input('id', sql.BigInt, id)
            .execute('GetById');

        result = data && data.recordset && data.recordset.length > 0
            ? data.recordset[0]
            : null;

        log.info(`Retreived URL data from DB for id ${id} : `, result);

    } catch (err) {
        log.error(`Error getting url data from sql for id ${id}: `, err);
    }
    finally{
        sql.close();
    }

    return result;
};

//Adds new short URL data into DB, or Retrieve existing record if longUrl matched.
const addOrGetUrlData = async (longUrl) => {
    log.info(`Adding or getting URL data to DB for long url ${longUrl}`);

    let result = null;
    try {
        const pool = await sql.connect(config.connectionString);
        const data = await pool.request()
            .input('longUrl', sql.NVarChar, longUrl)
            .execute('AddOrGetUrl');

        result = data && data.recordset && data.recordset.length > 0
            ? data.recordset[0]
            : null;

        log.info(`URL data added or retrived to DB for url ${longUrl}`, result);

    } catch (err) {
        log.error(`Error adding url data to sql for url ${longUrl}: `, err);
    }
    finally{
        sql.close();
    }

    return result;
};


module.exports = {
    getById,
    addOrGetUrlData
}