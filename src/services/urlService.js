const urlRepo = require('./urlRepository');
const baseConverter = require('./baseConverter');

//Resolves the orig long url given hash value
const resolveLongUrl = async (hash) => {
    const id = baseConverter.toNumber(hash);

    if(!id)
        return null;

    const data = await urlRepo.getById(id);

    return data && data.LongUrl
        ? data.LongUrl
        : null;
};

//Saves the long url into repository and return a lookup hash value
const shortenUrl = async (longUrl) => {

    if(!isUrlValid(longUrl) || longUrl.length > 2000)
       return null;

    const urlData = await urlRepo.addOrGetUrlData(longUrl);

    if(!urlData || !urlData.Id)
        return null;

    return baseConverter.toBase(urlData.Id);
};

const isUrlValid = (url) => {
    return !!(url || '').match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
}

module.exports = {
    resolveLongUrl,
    shortenUrl
}